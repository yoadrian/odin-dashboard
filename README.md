# Odin Admin Dashboard

Access the live page at: [https://yoadrian.gitlab.io/odin-dashboard/](https://yoadrian.gitlab.io/odin-dashboard/)
- the page, part of The Odin Project curriculum, is not meant to be responsive, mobile view is not optimal

## Project learning-points
- the biggest learning point has been the importance of planning ahead and thinking about the structure prior to implementation
- I created the page with fixed sidebar and header using modern CSS techniques, without removing these elements from the normal flow of the DOM, as explained by Paige Niedringhaus
- the page uses a mix of Fexbox and CSS Grid
- I experimented with responsive typography, thanks to CSS Tricks
- I learned about using inline SVGs, CSS custom properties, and browser compatibility
- SVG icons and avatars are from [https://pictogrammers.com/library/mdi/](https://pictogrammers.com/library/mdi/) and from [https://boringavatars.com/](https://boringavatars.com/)